import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService, PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [MessageService],
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  productNames: string[] = [
    "Bamboo Watch", 
    "Black Watch", 
    "Blue Band", 
    "Blue T-Shirt", 
    "Bracelet", 
    "Brown Purse", 
    "Chakra Bracelet",
    "Galaxy Earrings",
    "Game Controller",
    "Gaming Set",
    "Gold Phone Case",
    "Green Earbuds",
    "Green T-Shirt",
    "Grey T-Shirt",
    "Headphones",
    "Light Green T-Shirt",
    "Lime Band",
    "Mini Speakers",
    "Painted Phone Case",
    "Pink Band",
    "Pink Purse",
    "Purple Band",
    "Purple Gemstone Necklace",
    "Purple T-Shirt",
    "Shoes",
    "Sneakers",
    "Teal T-Shirt",
    "Yellow Earbuds",
    "Yoga Mat",
    "Yoga Set",
];

status: string[] = ['OUTOFSTOCK', 'INSTOCK', 'LOWSTOCK'];


  title = 'primeNgTable';
  columns: string[] = ["Produkt", "Gods type",
   "Pris kode", "Interval Fra", "Interval Til",
    "Afregnings Type", "Afregnings Enhed", "Pris",
     "Valuta", "Zone", "Tekst"];


     products1: any[] = [];

     products2: any[] = [];
 
     statuses: any[];
 
     clonedProducts: { [s: string]: any; } = {};


     constructor(private pimengConfig: PrimeNGConfig, private http: HttpClient,     private messageService: MessageService      ){
      for (let i = 0; i < 100; i++) {
        this.products1.push(this.generatePrduct());
        this.products2.push(this.generatePrduct());
      } 


      this.statuses = [{label: 'In Stock', value: 'INSTOCK'},{label: 'Low Stock', value: 'LOWSTOCK'},{label: 'Out of Stock', value: 'OUTOFSTOCK'}]
     }
  ngOnInit(): void {

  }

  generatePrduct(): any {
    const product: any =  {
        id: this.generateId(),
        name: this.generateName(),
        code: this.generateName(),
        description: "Product Description",
        price: this.generatePrice(),
        quantity: this.generateQuantity(),
        category: "Product Category",
        inventoryStatus: {label: 'In Stock', value: 'INSTOCK'},
        rating: this.generateRating(),
        text: this.generateName(),
        zone: this.generateName(),
        currency: this.generateName(),
        calcUnit: this.generateName(),
        calcType: this.generateName(),
        intervalTo: this.generateName()
    };

    product.image = product.name.toLocaleLowerCase().split(/[ ,]+/).join('-')+".jpg";;
    return product;
}

generateId() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
    for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    
    return text;
}

generateName() {
    return this.productNames[Math.floor(Math.random() * Math.floor(30))];
}

generatePrice() {
    return Math.floor(Math.random() * Math.floor(299)+1);
}

generateQuantity() {
    return Math.floor(Math.random() * Math.floor(75)+1);
}

generateStatus() {
    return this.status[Math.floor(Math.random() * Math.floor(3))];
}

generateRating() {
    return Math.floor(Math.random() * Math.floor(5)+1);
}


  onRowEditInit(product: any) {
}

onRowEditSave(product: any) {

}

onRowEditCancel(product: any, index: number) {

}
}
